package com.example.retrofit2test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ApiInterface apiService;
    String number;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiService =
                ApiClient.getClient().create(ApiInterface.class);


        number="10";
        registerUser();

        loginUser();
        Log.e("reached","eor");
        logout();
        Log.e("reached","eor");
        loginUser();
        //registerUser();
        Log.e("reached","eor");

        //logout();

        /*checkuser();
        Log.e("reached","eor");*/

        /*logout();
        Log.e("reached","eor");

        checkuser();
        Log.e("reached","eor");

        loginUser();
        Log.e("reached","eor");

        checkuser();
        Log.e("reached","eor");*/


    }

    public void checkuser(Headers cookie)
    {
        //String cookieString = cookie.toString().replaceAll("\n",";");
        Call<RequestResponse> call = apiService.checkUser(cookie.get("Set-Cookie"));
        call.enqueue(new Callback<RequestResponse>() {

            @Override
            public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                int statusCode = response.code();
                RequestResponse responses = response.body();

                Log.e("status code",statusCode + "");

                if(response.body()!=null) {
                    Log.e("response", response.body().toString());

                    Log.e("response", responses.getMessage());
                }

                //if(responses.getError()!=null)
                    //Log.e("response",responses.getError());
            }

            @Override
            public void onFailure(Call<RequestResponse> call, Throwable t) {
                Log.e("fail",t.toString());
                Log.e("response code", call.toString());
            }
        });
    }

    public void logout()
    {
        Call<RequestResponse> call = apiService.logout();
        call.enqueue(new Callback<RequestResponse>() {

            @Override
            public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                int statusCode = response.code();
                RequestResponse responses = response.body();

                Log.e("status code",statusCode+"");

                Log.e("response",response.body().toString());

                Log.e("response",responses.getMessage());

                if(responses.getError()!=null)
                    Log.e("response",responses.getError());

                checkuser(response.headers());
            }

            @Override
            public void onFailure(Call<RequestResponse> call, Throwable t) {
                Log.e("fail",t.toString());
                Log.e("response code", call.toString());
            }
        });
    }

    public void registerUser()
    {
        Call<RequestResponse> call = apiService.registerUser("eugen.vlasie" + number + "@email.com","parola","1","Eugen Vlasie");
        call.enqueue(new Callback<RequestResponse>() {

            @Override
            public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {
                int statusCode = response.code();
                RequestResponse responses = response.body();

                Log.e("status code",statusCode+"");

                Log.e("response",response.body().toString());

                Log.e("response",responses.getMessage());

                if(responses.getError()!=null)
                    Log.e("response",responses.getError());
            }

            @Override
            public void onFailure(Call<RequestResponse> call, Throwable t) {
                Log.e("fail",t.toString());
                Log.e("response code", call.toString());
            }
        });
    }

    public void loginUser()
    {
        Call<RequestResponse> call = apiService.loginUser("eugen.vlasie" + number + "@email.com","parola","1");
        call.enqueue(new Callback<RequestResponse>() {

            @Override
            public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

                int statusCode = response.code();

                RequestResponse responses = response.body();


                Log.e("status code",statusCode+"");

                Log.e("response",response.body().toString());

                Log.e("response",responses.getMessage());

                if(responses.getError()!=null)
                    Log.e("response",responses.getError());

                //logout();

                checkuser(response.headers());
                Log.e("reached","eor");
            }

            @Override
            public void onFailure(Call<RequestResponse> call, Throwable t) {
                Log.e("fail",t.toString());
                Log.e("response code", call.toString());
            }
        });
    }
}
