package com.example.retrofit2test;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cosminalex on 13.06.2017.
 */


class ApiClient {

    private static final String BASE_URL ="http://13.82.225.19:5001/";

    private static Retrofit retrofit = null;

    static  Retrofit getClient() {
        if(retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
