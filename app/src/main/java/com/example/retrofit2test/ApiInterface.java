package com.example.retrofit2test;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

interface ApiInterface {

    @FormUrlEncoded
    @PUT("/user")
    Call<RequestResponse> registerUser(@Field("mail") String mail,@Field("key") String key,@Field("auth_method") String auth_method,@Field("full_name") String full_name);

    @GET("/user")
    Call<RequestResponse> checkUser(@Header("Cookie") String cookie);

    @POST("/logout")
    Call<RequestResponse> logout();

    @FormUrlEncoded
    @POST("/login")
    Call<RequestResponse> loginUser(@Field("mail") String mail,@Field("key") String key,@Field("auth_method") String auth_method);
}
